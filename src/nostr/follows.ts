import { Set } from 'npm:immutable';

import { cipher, followsDB } from '@/db.ts';
import { isPubkeyTag } from '@/nostr/tags.ts';
import { isURL } from '@/utils/parse.ts';

import type { Event } from '@/nostr/event.ts';

// FIXME: this is not efficient!
function getFollowsDiff(event: Event<3>) {
  const last = followsDB
    .getFollows(event.pubkey)
    .filter(isURL);

  const next = event.tags
    .filter(isPubkeyTag)
    .map((tag) => cipher.getApId(tag[1]))
    .filter(isURL);

  return {
    follow: Set(next).subtract(last).toArray(),
    unfollow: Set(last).subtract(next).toArray(),
  };
}

export { getFollowsDiff };
