import { Context } from '../../deps.ts';
import { maybeAddContext } from '../utils.ts';

function activityJson<T, P extends string>(c: Context<P>, object: T) {
  const response = c.json(maybeAddContext(object));
  response.headers.set('content-type', 'application/activity+json; charset=UTF-8');
  return response;
}

export { activityJson };
