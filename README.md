# Mostr

**/ ˈmoʊ stər /**, Mastodon and Other Software Transmitted over Relays.

Mostr is a bridge between Nostr and the Fediverse (Mastodon, ActivityPub, etc). It allows users on both networks to communicate, through a Mostr server.

On the ActivityPub side, you'll see the user's public key in mentions and URLs:

- Mention: `@28fa2c82cdf60a8c06b94f2482e5de9f92e5cd03b03140dea5e1c3dc173dbf41@mostr.pub`
- URL: `https://mostr.pub/objects/e3460f9dbe3c41c7e2e26290d08b39ee997a1f1a42702f5db6385369ff6b7093`

On the Nostr side, you'll see:

- NIP-05: `alex_at_gleasonator.com@mostr.pub`
- Normal pubkeys and events.

Nostr users should subscribe to our relay at `wss://relay.mostr.pub`. They can also locate users by searching for the NIP-05 identifier.

ActivityPub users will slowly discover Nostr users over time in the Local and Fediverse timelines. They can also search for these URLs:

- User: `https://mostr.pub/users/<pubkey>`
- Post: `https://mostr.pub/objects/<eventid>`

## Implementation

The following table shows supported actions, and how they map to each other.

| ActivityPub | Nostr |
|-------------|-------|
| Follow | Event<3> |
| Create(Note) | Event<1> |
| Announce(Note) | Event<1> |
| Update(Actor) | Event<0> |
| Like(Note) | Event<7> |
| EmojiReact(Note) | Event<7> |
| Delete(Note) | Event<5> |

### Supported NIPs

Some NIPs have partial support.

- [x] NIP-01: Basic protocol flow description
- [x] NIP-02: Contact List and Petnames
- [ ] NIP-03: OpenTimestamps Attestations for Events
- [ ] NIP-04: Encrypted Direct Message
- [x] NIP-05: Mapping Nostr keys to DNS-based internet identifiers
- [ ] NIP-06: Basic key derivation from mnemonic seed phrase
- [ ] NIP-07: `window.nostr` capability for web browsers
- [x] NIP-08: Handling Mentions
- [x] NIP-09: Event Deletion
- [x] NIP-10: Conventions for clients' use of `e` and `p` tags in text events
- [ ] NIP-11: Relay Information Document
- [ ] NIP-12: Generic Tag Queries
- [ ] NIP-13: Proof of Work
- [ ] NIP-14: Subject tag in text events.
- [ ] NIP-15: End of Stored Events Notice
- [ ] NIP-16: Event Treatment
- [x] NIP-18: Reposts
- [ ] NIP-19: bech32-encoded entities
- [ ] NIP-20: Command Results
- [ ] NIP-21: `nostr:` URL scheme
- [ ] NIP-22: Event `created_at` Limits
- [ ] NIP-23: Long-form Content
- [x] NIP-25: Reactions
- [ ] NIP-26: Delegated Event Signing
- [x] NIP-27: Text Note References
- [ ] NIP-28: Public Chat
- [ ] NIP-33: Parameterized Replaceable Events
- [x] NIP-36: Sensitive Content
- [ ] NIP-40: Expiration Timestamp
- [ ] NIP-42: Authentication of clients to relays
- [ ] NIP-46: Nostr Connect
- [ ] NIP-50: Keywords filter
- [ ] NIP-56: Reporting
- [x] NIP-57: Lightning Zaps
- [ ] NIP-58: Badges
- [ ] NIP-65: Relay List Metadata

### How it works

Mostr is an ActivityPub _server_ and a Nostr _client_.

```mermaid
sequenceDiagram
    ActivityPub->>Mostr: POST /inbox
    Mostr->>Nostr Relay: EVENT, REQ
    Nostr Relay->>Mostr: EVENT
    Mostr->>ActivityPub: POST /inbox
```

Mostr subscribes to a relay pool and federates any relevant events. It listens on its ActivityPub inbox and sends messages to other inboxes.

#### Tech stack

Mostr is built with TypeScript and [Deno](https://deno.land/). It uses an SQLite database and makes heavy use of the web [Cache API](https://developer.mozilla.org/en-US/docs/Web/API/Cache). Private keys are generated deterministically for bridged accounts, and very little data is actually stored by the bridge. The relay is the data store.

To get up and running, install deno and run `deno task dev`.

## Why?

See: https://soapbox.pub/blog/mostr-fediverse-nostr-bridge/

# Support us

If you like what we do, please consider donating to support our efforts. We cannot do this without the support of the community. Thank you!

- BTC: `bc1q9cx35adpm73aq2fw40ye6ts8hfxqzjr5unwg0n`
- https://soapbox.pub/donate

# License

Mostr is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mostr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Soapbox. If not, see <https://www.gnu.org/licenses/>.
